import * as puppeteer from 'puppeteer';

export async function isVisible(page: puppeteer.Page, selectorOrElementHandle: string | puppeteer.ElementHandle) {
    const element = typeof selectorOrElementHandle === 'string'
        ? await page.$(selectorOrElementHandle)
        : selectorOrElementHandle;
    if (element === null) {
        throw new Error(`Failed to find object`);
    }

    return page.evaluate((element: Element) => {
        const style = getComputedStyle(element);
        const rect = element.getBoundingClientRect();

        return style.visibility !== 'hidden' && !!(rect.bottom || rect.top || rect.height || rect.width) && style.opacity !== '0';
    }, element);
}
