import * as puppeteer from 'puppeteer';
export declare function assertSuccessfulSelector(selector: string, element: puppeteer.ElementHandle | null): puppeteer.ElementHandle<Element>;
