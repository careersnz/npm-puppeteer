"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.findElementWithText = void 0;
const get_text_1 = require("./get-text");
/**
 * Tries to find an element containing the specified text in its innerText value.
 * @param page The puppeteer page.
 * @param elementSelector The set of elements to search in.
 * @param text The text to search for. This value is case-sensitive.
 */
function findElementWithText(page, elementSelector, text) {
    return __awaiter(this, void 0, void 0, function* () {
        const elements = (yield page.$$(elementSelector));
        for (const element of elements) {
            const textOfElement = yield get_text_1.getText(page, element);
            if (textOfElement && textOfElement.includes(text)) {
                return element;
            }
        }
        return null;
    });
}
exports.findElementWithText = findElementWithText;
;
