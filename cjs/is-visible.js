"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.isVisible = void 0;
function isVisible(page, selectorOrElementHandle) {
    return __awaiter(this, void 0, void 0, function* () {
        const element = typeof selectorOrElementHandle === 'string'
            ? yield page.$(selectorOrElementHandle)
            : selectorOrElementHandle;
        if (element === null) {
            throw new Error(`Failed to find object`);
        }
        return page.evaluate((element) => {
            const style = getComputedStyle(element);
            const rect = element.getBoundingClientRect();
            return style.visibility !== 'hidden' && !!(rect.bottom || rect.top || rect.height || rect.width) && style.opacity !== '0';
        }, element);
    });
}
exports.isVisible = isVisible;
