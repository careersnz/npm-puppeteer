import * as puppeteer from 'puppeteer';
export declare function isVisible(page: puppeteer.Page, selectorOrElementHandle: string | puppeteer.ElementHandle): Promise<any>;
