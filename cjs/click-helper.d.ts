import * as puppeteer from 'puppeteer';
/**
 * Clicks an element and waits for the page to navigate to a new URL or to reload.
 * @param page The puppeteer page.
 * @param elementOrSelector The HTMLElement or string selector to click.
 * @param waitOptions Optional wait settings.
 * @returns A promise which resolves after the page has navigated.
 */
export declare function clickAndWaitForNavigation(page: puppeteer.Page, elementOrSelector: string | puppeteer.ElementHandle, waitOptions?: puppeteer.WaitForOptions): Promise<[puppeteer.HTTPResponse | null, void]>;
export declare function clickSelectorAndWaitForNavigation(page: puppeteer.Page, selector: string, waitOptions?: puppeteer.WaitForOptions): Promise<[puppeteer.HTTPResponse | null, void]>;
export declare function clickElementAndWaitForNavigation(page: puppeteer.Page, element: puppeteer.ElementHandle, waitOptions?: puppeteer.WaitForOptions): Promise<[puppeteer.HTTPResponse | null, void]>;
