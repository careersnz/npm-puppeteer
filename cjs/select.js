"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.uncheckAll = exports.getTextsOfSelectedOptions = exports.getTextOfSelectedOption = exports.selectWithText = void 0;
const throw_helper_1 = require("./throw-helper");
/**
 * Selects one (or many) dropdown items by matching their <option /> text values.
 * @param page The puppeteer page.
 * @param selector A selector to a <select /> element.
 * @param  texts A set of <option /> innerText values to match on.
 * @returns An array of option values that have been successfully selected.
 */
function selectWithText(page, selector, ...texts) {
    return __awaiter(this, void 0, void 0, function* () {
        const select = (yield page.$(selector));
        throw_helper_1.assertSuccessfulSelector(selector, select);
        const options = yield select.$$('option');
        // Pull out just the option value and text
        const optionData = (yield Promise.all(options.map((opt) => __awaiter(this, void 0, void 0, function* () {
            var _a;
            const value = yield page.evaluate(element => element.value, opt);
            const text = (_a = (yield page.evaluate(element => element.textContent, opt))) === null || _a === void 0 ? void 0 : _a.trim();
            return {
                value: value.toString(),
                text
            };
        })))).filter(opt => opt.text && texts.includes(opt.text));
        return select.select(...optionData.map(opt => opt.value));
    });
}
exports.selectWithText = selectWithText;
/**
 * Gets the text of the currently selected option of a <select />.
 * @param page The puppeteer page.
 * @param selector A selector to a <select /> element.
 * @returns
 */
function getTextOfSelectedOption(page, selector) {
    return __awaiter(this, void 0, void 0, function* () {
        const select = (yield page.$(selector));
        throw_helper_1.assertSuccessfulSelector(selector, select);
        return page.evaluate((select) => {
            return select.options[select.selectedIndex].text;
        }, select);
    });
}
exports.getTextOfSelectedOption = getTextOfSelectedOption;
/**
 * Gets the set of text of the currently selected options of a <select /> element.
 * @param page The puppeteer page.
 * @param selector A selector to a <select /> element.
 */
function getTextsOfSelectedOptions(page, selector) {
    return __awaiter(this, void 0, void 0, function* () {
        return yield page.evaluate((selector_) => {
            const select = document.querySelector(selector_);
            if (!select) {
                throw new Error(`Could find select element by '${selector}'`);
            }
            return Array.from(select.options)
                .filter(opt => opt.selected)
                .map(opt => opt.innerText);
        }, selector);
    });
}
exports.getTextsOfSelectedOptions = getTextsOfSelectedOptions;
/**
 * Uncheck all options of a select.
 * @param page The puppeteer page.
 * @param selector A selector to a <select /> element.
 */
function uncheckAll(page, selector) {
    return __awaiter(this, void 0, void 0, function* () {
        yield page.evaluate((selector_) => {
            const select = document.querySelector(selector_);
            if (!select) {
                throw new Error(`Could not find select with selector '${selector_}'`);
            }
            for (const opt of select.options) {
                opt.selected = false;
            }
        }, selector);
    });
}
exports.uncheckAll = uncheckAll;
