import * as puppeteer from 'puppeteer';
/**
 * Is a checkbox checked?
 * @param selector The selector for the HTMLInputElement element.
 * @param page The puppeteer page.
 * @returns true if the checkbox is checked, false otherwise.
 */
export declare function isChecked(page: puppeteer.Page, selector: string): Promise<boolean>;
/**
 * Checks a checkbox.
 * @param page The puppeteer page.
 * @param selector The selector for the HTMLInputElement element.
 */
export declare function check(page: puppeteer.Page, selector: string): Promise<void>;
/**
 * Unchecks a checkbox.
 * @param page The puppeteer page.
 * @param selector The selector for the HTMLInputElement element.
 */
export declare function uncheck(page: puppeteer.Page, selector: string): Promise<void>;
