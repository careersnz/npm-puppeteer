"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.clickElementAndWaitForNavigation = exports.clickSelectorAndWaitForNavigation = exports.clickAndWaitForNavigation = void 0;
/**
 * Clicks an element and waits for the page to navigate to a new URL or to reload.
 * @param page The puppeteer page.
 * @param elementOrSelector The HTMLElement or string selector to click.
 * @param waitOptions Optional wait settings.
 * @returns A promise which resolves after the page has navigated.
 */
function clickAndWaitForNavigation(page, elementOrSelector, waitOptions) {
    if (typeof elementOrSelector === 'string') {
        return clickSelectorAndWaitForNavigation(page, elementOrSelector, waitOptions);
    }
    else {
        return clickElementAndWaitForNavigation(page, elementOrSelector, waitOptions);
    }
}
exports.clickAndWaitForNavigation = clickAndWaitForNavigation;
function clickSelectorAndWaitForNavigation(page, selector, waitOptions) {
    return __awaiter(this, void 0, void 0, function* () {
        return Promise.all([
            page.waitForNavigation(waitOptions),
            page.click(selector)
        ]);
    });
}
exports.clickSelectorAndWaitForNavigation = clickSelectorAndWaitForNavigation;
function clickElementAndWaitForNavigation(page, element, waitOptions) {
    return __awaiter(this, void 0, void 0, function* () {
        return Promise.all([
            page.waitForNavigation(waitOptions),
            element.click()
        ]);
    });
}
exports.clickElementAndWaitForNavigation = clickElementAndWaitForNavigation;
