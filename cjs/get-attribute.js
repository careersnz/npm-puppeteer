"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getAttribute = void 0;
const throw_helper_1 = require("./throw-helper");
/**
 * Get the attirbute value from an element.
 * @param page The puppeteer page.
 * @param elementSelectorOrHandle An element selector or handle to an element.
 * @param attributeName The name of the attribute to get the value for.
 */
function getAttribute(page, elementSelectorOrHandle, attributeName) {
    return __awaiter(this, void 0, void 0, function* () {
        let element;
        switch (typeof elementSelectorOrHandle) {
            case 'string':
                element = (yield page.waitForSelector(elementSelectorOrHandle));
                throw_helper_1.assertSuccessfulSelector(elementSelectorOrHandle, element);
                break;
            case 'object':
                element = elementSelectorOrHandle;
                break;
            default:
                throw new Error(`Unsupported type ${typeof elementSelectorOrHandle}`);
        }
        return yield page.evaluate((el, att) => el.getAttribute(att), element, attributeName);
    });
}
exports.getAttribute = getAttribute;
;
