import * as puppeteer from 'puppeteer';
/**
 * Gets the value from an HTMLInputElement.
 * @param page The puppeteer page.
 * @param elementSelectorOrHandle A selector for a <input /> element, or a handle.
 */
export declare function getValue(page: puppeteer.Page, elementSelectorOrHandle: string | puppeteer.ElementHandle<HTMLInputElement>): Promise<any>;
/**
 * @param selector A selector for a <input /> element.
 * @param text The text to type into the <input />.
 */
export declare function clearAndType(page: puppeteer.Page, selector: string, text: string): Promise<void>;
