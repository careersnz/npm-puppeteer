import * as puppeteer from 'puppeteer';
/**
 * Tries to find an element containing the specified text in its innerText value.
 * @param page The puppeteer page.
 * @param elementSelector The set of elements to search in.
 * @param text The text to search for. This value is case-sensitive.
 */
export declare function findElementWithText(page: puppeteer.Page, elementSelector: string, text: string): Promise<puppeteer.ElementHandle<HTMLElement> | null>;
