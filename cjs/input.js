"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.clearAndType = exports.getValue = void 0;
const throw_helper_1 = require("./throw-helper");
/**
 * Gets the value from an HTMLInputElement.
 * @param page The puppeteer page.
 * @param elementSelectorOrHandle A selector for a <input /> element, or a handle.
 */
function getValue(page, elementSelectorOrHandle) {
    return __awaiter(this, void 0, void 0, function* () {
        switch (typeof elementSelectorOrHandle) {
            case 'string':
                const element = (yield page.waitForSelector(elementSelectorOrHandle));
                throw_helper_1.assertSuccessfulSelector(elementSelectorOrHandle, element);
                return page.evaluate(el => el.value, element);
            case 'object':
                return page.evaluate(el => el.value, elementSelectorOrHandle);
            default:
                throw new Error(`Unsupported type ${typeof elementSelectorOrHandle}`);
        }
    });
}
exports.getValue = getValue;
/**
 * @param selector A selector for a <input /> element.
 * @param text The text to type into the <input />.
 */
function clearAndType(page, selector, text) {
    return __awaiter(this, void 0, void 0, function* () {
        const input = (yield page.$(selector));
        throw_helper_1.assertSuccessfulSelector(selector, input);
        yield page.evaluate(i => {
            i.value = '';
        }, input);
        yield page.type(selector, text);
    });
}
exports.clearAndType = clearAndType;
