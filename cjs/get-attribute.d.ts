import * as puppeteer from 'puppeteer';
/**
 * Get the attirbute value from an element.
 * @param page The puppeteer page.
 * @param elementSelectorOrHandle An element selector or handle to an element.
 * @param attributeName The name of the attribute to get the value for.
 */
export declare function getAttribute(page: puppeteer.Page, elementSelectorOrHandle: string | puppeteer.ElementHandle<Element>, attributeName: string): Promise<any>;
