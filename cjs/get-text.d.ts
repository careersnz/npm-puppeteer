import * as puppeteer from 'puppeteer';
/**
 * Gets the innerText value of an element.
 * @param page The puppeteer page.
 * @param elementSelectorOrHandle The HTMLElement or string selector to get the innerText value from.
 */
export declare function getText(page: puppeteer.Page, elementSelectorOrHandle: string | puppeteer.ElementHandle<HTMLElement>): Promise<string | null | undefined>;
