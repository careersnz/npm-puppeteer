"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getText = void 0;
const throw_helper_1 = require("./throw-helper");
/**
 * Gets the innerText value of an element.
 * @param page The puppeteer page.
 * @param elementSelectorOrHandle The HTMLElement or string selector to get the innerText value from.
 */
function getText(page, elementSelectorOrHandle) {
    return __awaiter(this, void 0, void 0, function* () {
        switch (typeof elementSelectorOrHandle) {
            case 'string':
                const element = (yield page.waitForSelector(elementSelectorOrHandle));
                throw_helper_1.assertSuccessfulSelector(elementSelectorOrHandle, element);
                return (page.evaluate(el => el.innerText, element));
            case 'object':
                return (page.evaluate(el => el.innerText, elementSelectorOrHandle));
            default:
                throw new Error(`Unsupported type ${typeof elementSelectorOrHandle}`);
        }
    });
}
exports.getText = getText;
