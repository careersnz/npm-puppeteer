"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.uncheck = exports.check = exports.isChecked = void 0;
/**
 * Is a checkbox checked?
 * @param selector The selector for the HTMLInputElement element.
 * @param page The puppeteer page.
 * @returns true if the checkbox is checked, false otherwise.
 */
function isChecked(page, selector) {
    return __awaiter(this, void 0, void 0, function* () {
        const isChecked = yield page.evaluate((selector_) => {
            const checkbox = document.querySelector(selector_);
            if (!checkbox) {
                return null;
            }
            return checkbox.checked;
        }, selector);
        if (isChecked === null) {
            throw new Error(`Failed to get the checked value of '${selector}'`);
        }
        return isChecked;
    });
}
exports.isChecked = isChecked;
/**
 * Checks a checkbox.
 * @param page The puppeteer page.
 * @param selector The selector for the HTMLInputElement element.
 */
function check(page, selector) {
    return __awaiter(this, void 0, void 0, function* () {
        if (!(yield isChecked(page, selector))) {
            yield page.click(selector);
            if (!(yield isChecked(page, selector))) {
                throw new Error(`Expected checkbox '${selector}' to be checked`);
            }
        }
    });
}
exports.check = check;
/**
 * Unchecks a checkbox.
 * @param page The puppeteer page.
 * @param selector The selector for the HTMLInputElement element.
 */
function uncheck(page, selector) {
    return __awaiter(this, void 0, void 0, function* () {
        if (yield isChecked(page, selector)) {
            yield page.click(selector);
            if (yield isChecked(page, selector)) {
                throw new Error(`Expected checkbox '${selector}' to be unchecked`);
            }
        }
    });
}
exports.uncheck = uncheck;
