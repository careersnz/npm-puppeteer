"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MailHog = void 0;
const node_fetch_1 = __importDefault(require("node-fetch"));
const mailparser_1 = __importDefault(require("mailparser"));
class MailHog {
    constructor(mailHogUrl) {
        this._mailHogUrl = mailHogUrl;
    }
    /**
     * Gets email from the mailhog inbox which match the specified 'to' address.
     * @param addressNeedle A 'to' address filter.
     */
    get(addressNeedle) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield node_fetch_1.default(`${this._mailHogUrl}/api/v2/search?kind=to&query=${encodeURIComponent(addressNeedle)}`);
            const messagesJson = yield response.json();
            const messages = messagesJson.items;
            return Promise.all(messages.map(message => mailparser_1.default.simpleParser(message.Raw.Data)));
        });
    }
}
exports.MailHog = MailHog;
