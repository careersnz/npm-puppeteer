import * as puppeteer from 'puppeteer';
export declare function waitUntil(page: puppeteer.Page, waitFunction: () => Promise<boolean>, errorMessage?: string, timeOut?: number, pollInterval?: number): Promise<void>;
