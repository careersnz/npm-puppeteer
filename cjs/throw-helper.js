"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.assertSuccessfulSelector = void 0;
function assertSuccessfulSelector(selector, element) {
    if (element === null) {
        throw new Error(`Failed to find object by selector '${selector}'`);
    }
    return element;
}
exports.assertSuccessfulSelector = assertSuccessfulSelector;
