import parseEmail from 'mailparser';
export declare class MailHog {
    private _mailHogUrl;
    constructor(mailHogUrl: string);
    /**
     * Gets email from the mailhog inbox which match the specified 'to' address.
     * @param addressNeedle A 'to' address filter.
     */
    get(addressNeedle: string): Promise<parseEmail.ParsedMail[]>;
}
