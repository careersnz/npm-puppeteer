import * as puppeteer from 'puppeteer';
/**
 * Selects one (or many) dropdown items by matching their <option /> text values.
 * @param page The puppeteer page.
 * @param selector A selector to a <select /> element.
 * @param  texts A set of <option /> innerText values to match on.
 * @returns An array of option values that have been successfully selected.
 */
export declare function selectWithText(page: puppeteer.Page, selector: string, ...texts: string[]): Promise<string[]>;
/**
 * Gets the text of the currently selected option of a <select />.
 * @param page The puppeteer page.
 * @param selector A selector to a <select /> element.
 * @returns
 */
export declare function getTextOfSelectedOption(page: puppeteer.Page, selector: string): Promise<any>;
/**
 * Gets the set of text of the currently selected options of a <select /> element.
 * @param page The puppeteer page.
 * @param selector A selector to a <select /> element.
 */
export declare function getTextsOfSelectedOptions(page: puppeteer.Page, selector: string): Promise<any>;
/**
 * Uncheck all options of a select.
 * @param page The puppeteer page.
 * @param selector A selector to a <select /> element.
 */
export declare function uncheckAll(page: puppeteer.Page, selector: string): Promise<void>;
