import * as puppeteer from 'puppeteer';
import { getText } from './get-text';

/**
 * Tries to find an element containing the specified text in its innerText value.
 * @param page The puppeteer page.
 * @param elementSelector The set of elements to search in.
 * @param text The text to search for. This value is case-sensitive.
 */
export async function findElementWithText(page: puppeteer.Page, elementSelector: string, text: string) {
    const elements = (await page.$$(elementSelector)) as puppeteer.ElementHandle<HTMLElement>[];
    for (const element of elements) {
        const textOfElement = await getText(page, element);
        if (textOfElement && textOfElement.includes(text)) {
            return element;
        }
    }
    return null;
};
