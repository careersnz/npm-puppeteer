import * as puppeteer from 'puppeteer';
import { assertSuccessfulSelector } from './throw-helper';

/**
 * Get the attirbute value from an element.
 * @param page The puppeteer page.
 * @param elementSelectorOrHandle An element selector or handle to an element.
 * @param attributeName The name of the attribute to get the value for.
 */
export async function getAttribute (page: puppeteer.Page, elementSelectorOrHandle: string | puppeteer.ElementHandle<Element>, attributeName: string) {
    let element: puppeteer.ElementHandle<Element>;
    switch (typeof elementSelectorOrHandle) {
    case 'string':
        element = (await page.waitForSelector(elementSelectorOrHandle))!;
        assertSuccessfulSelector(elementSelectorOrHandle, element);
        break;
    case 'object':
        element = elementSelectorOrHandle;
        break;
    default:
        throw new Error(`Unsupported type ${typeof elementSelectorOrHandle}`);
    }

    return await page.evaluate((el, att) => el.getAttribute(att), element, attributeName);
};
