import * as puppeteer from 'puppeteer';

export async function waitUntil(page: puppeteer.Page, waitFunction: () => Promise<boolean>, errorMessage = 'Timed out waiting for something', timeOut = 5000, pollInterval = 100) {
    const endTime = Date.now() + timeOut;
    let conditionMet = await waitFunction();
    while (!conditionMet && Date.now() < endTime) {
        await page.waitForTimeout(pollInterval);
        conditionMet = await waitFunction();
    }
    if (!conditionMet) {
        throw new Error(errorMessage);
    }
}
