import * as puppeteer from 'puppeteer';
import { assertSuccessfulSelector } from './throw-helper';

/**
 * Gets the value from an HTMLInputElement.
 * @param page The puppeteer page.
 * @param elementSelectorOrHandle A selector for a <input /> element, or a handle.
 */
export async function getValue (page: puppeteer.Page, elementSelectorOrHandle: string | puppeteer.ElementHandle<HTMLInputElement>) {
    switch (typeof elementSelectorOrHandle) {
    case 'string':
        const element = (await page.waitForSelector(elementSelectorOrHandle))! as puppeteer.ElementHandle<HTMLInputElement>;
        assertSuccessfulSelector(elementSelectorOrHandle, element);
        return page.evaluate(el => el.value, element);
    case 'object':
        return page.evaluate(el => el.value, elementSelectorOrHandle);
    default:
        throw new Error(`Unsupported type ${typeof elementSelectorOrHandle}`);
    }
}

/**
 * @param selector A selector for a <input /> element.
 * @param text The text to type into the <input />.
 */
export async function clearAndType (page: puppeteer.Page, selector: string, text: string) {
    const input = (await page.$(selector))! as puppeteer.ElementHandle<HTMLInputElement>;
    assertSuccessfulSelector(selector, input);

    await page.evaluate(i => {
        i.value = '';
    }, input);

    await page.type(selector, text);
}
