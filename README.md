# Puppeteer helper packages
## Getting started
```
npm ci
```
## Dev
Helpers are written in typescript and then transpiled out to various formats and module systems (currently only CJS).

### Build
Run the following command. This will emit the `cjs` directory.
```
npx tsc
```

## Use
`npm` has support for `bitbucket` package sources (https://docs.npmjs.com/cli/v7/commands/npm-install)

So to install the `@tecgovtnz/puppeteer` package, run:
```
npm i bitbucket:careersnz/npm-puppeteer
```

If you are developing a new package and which to test your branch you can use the syntax:
```
npm i bitbucket:careersnz/npm-puppeteer\#BRANCH_NAME
```