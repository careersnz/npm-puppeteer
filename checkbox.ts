import * as puppeteer from 'puppeteer';

/**
 * Is a checkbox checked?
 * @param selector The selector for the HTMLInputElement element.
 * @param page The puppeteer page.
 * @returns true if the checkbox is checked, false otherwise.
 */
export async function isChecked (page: puppeteer.Page, selector: string) : Promise<boolean> {
    const isChecked = await page.evaluate((selector_: string) => {
        const checkbox = document.querySelector(selector_) as HTMLInputElement;
        if (!checkbox) {
            return null;
        }
        return checkbox.checked;
    }, selector);

    if (isChecked === null) {
        throw new Error(`Failed to get the checked value of '${selector}'`);
    }

    return isChecked;
}

/**
 * Checks a checkbox.
 * @param page The puppeteer page.
 * @param selector The selector for the HTMLInputElement element.
 */
export async function check (page: puppeteer.Page, selector: string) {
    if (!(await isChecked(page, selector))) {
        await page.click(selector);

        if (!(await isChecked(page, selector))) {
            throw new Error(`Expected checkbox '${selector}' to be checked`);
        }
    }
}

/**
 * Unchecks a checkbox.
 * @param page The puppeteer page.
 * @param selector The selector for the HTMLInputElement element.
 */
export async function uncheck (page: puppeteer.Page, selector: string) {
    if (await isChecked(page, selector)) {
        await page.click(selector);

        if (await isChecked(page, selector)) {
            throw new Error(`Expected checkbox '${selector}' to be unchecked`);
        }
    }
}
