import * as puppeteer from 'puppeteer';
import { assertSuccessfulSelector } from './throw-helper';

/**
 * Gets the innerText value of an element.
 * @param page The puppeteer page.
 * @param elementSelectorOrHandle The HTMLElement or string selector to get the innerText value from.
 */
export async function getText(page: puppeteer.Page, elementSelectorOrHandle: string | puppeteer.ElementHandle<HTMLElement>) {
    switch (typeof elementSelectorOrHandle) {
    case 'string':
        const element = (await page.waitForSelector(elementSelectorOrHandle))! as puppeteer.ElementHandle<HTMLElement>;
        assertSuccessfulSelector(elementSelectorOrHandle, element);

        return (page.evaluate(el => el.innerText, element)) as Promise<string | null | undefined>;
    case 'object':
        return (page.evaluate(el => el.innerText, elementSelectorOrHandle)) as Promise<string | null | undefined>;
    default:
        throw new Error(`Unsupported type ${typeof elementSelectorOrHandle}`);
    }
}
