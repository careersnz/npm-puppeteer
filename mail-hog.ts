import nodeFetch from 'node-fetch';
import parseEmail from 'mailparser';

export class MailHog {
    private _mailHogUrl: string;

    constructor (mailHogUrl: string) {
        this._mailHogUrl = mailHogUrl;
    }

    /**
     * Gets email from the mailhog inbox which match the specified 'to' address.
     * @param addressNeedle A 'to' address filter.
     */
    async get (addressNeedle: string) {
        const response = await nodeFetch(`${this._mailHogUrl}/api/v2/search?kind=to&query=${encodeURIComponent(addressNeedle)}`);
        const messagesJson = await response.json();

        const messages = messagesJson.items as any[];

        return Promise.all(messages.map(message => parseEmail.simpleParser(message.Raw.Data)));
    }
}
