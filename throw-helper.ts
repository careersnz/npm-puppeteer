import * as puppeteer from 'puppeteer';

export function assertSuccessfulSelector(selector: string, element: puppeteer.ElementHandle | null) {
    if (element === null) {
        throw new Error(`Failed to find object by selector '${selector}'`);
    }
    return element;
}
