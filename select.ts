import * as puppeteer from 'puppeteer';
import { assertSuccessfulSelector } from './throw-helper';

/**
 * Selects one (or many) dropdown items by matching their <option /> text values.
 * @param page The puppeteer page.
 * @param selector A selector to a <select /> element.
 * @param  texts A set of <option /> innerText values to match on.
 * @returns An array of option values that have been successfully selected.
 */
export async function selectWithText (page: puppeteer.Page, selector: string, ...texts: string[]) {
    const select = (await page.$(selector))!;
    assertSuccessfulSelector(selector, select);

    const options = await select.$$('option');

    // Pull out just the option value and text
    const optionData = (await Promise.all(
        options.map(async opt => {
            const value = await page.evaluate(element => element.value, opt);
            const text = (await page.evaluate(element => element.textContent, opt))?.trim();

            return {
                value: value.toString(),
                text
            };
        })
    )).filter(opt => opt.text && texts.includes(opt.text));

    return select.select(...optionData.map(opt => opt.value));
}

/**
 * Gets the text of the currently selected option of a <select />.
 * @param page The puppeteer page.
 * @param selector A selector to a <select /> element.
 * @returns
 */
 export async function getTextOfSelectedOption (page: puppeteer.Page, selector: string) {
    const select = (await page.$(selector))!;
    assertSuccessfulSelector(selector, select);
    return page.evaluate((select: HTMLSelectElement) => {
        return select.options[select.selectedIndex].text;
    }, select as puppeteer.ElementHandle<HTMLSelectElement>);
}

/**
 * Gets the set of text of the currently selected options of a <select /> element.
 * @param page The puppeteer page.
 * @param selector A selector to a <select /> element.
 */
export async function getTextsOfSelectedOptions (page: puppeteer.Page, selector: string) {
    return await page.evaluate((selector_: string) => {
        const select = document.querySelector(selector_) as HTMLSelectElement | null;

        if (!select) {
            throw new Error(`Could find select element by '${selector}'`);
        }

        return Array.from(select.options)
            .filter(opt => opt.selected)
            .map(opt => opt.innerText);
    }, selector);
}

/**
 * Uncheck all options of a select.
 * @param page The puppeteer page.
 * @param selector A selector to a <select /> element.
 */
export async function uncheckAll (page: puppeteer.Page, selector: string) {
    await page.evaluate((selector_: string) => {
        const select = document.querySelector(selector_) as HTMLSelectElement | null;
        if (!select) {
            throw new Error(`Could not find select with selector '${selector_}'`);
        }

        for (const opt of select.options) {
            opt.selected = false;
        }
    }, selector);
}
