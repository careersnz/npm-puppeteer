import * as puppeteer from 'puppeteer';

/**
 * Clicks an element and waits for the page to navigate to a new URL or to reload.
 * @param page The puppeteer page.
 * @param elementOrSelector The HTMLElement or string selector to click.
 * @param waitOptions Optional wait settings.
 * @returns A promise which resolves after the page has navigated.
 */
export function clickAndWaitForNavigation (page: puppeteer.Page, elementOrSelector: string | puppeteer.ElementHandle, waitOptions?: puppeteer.WaitForOptions) {
    if (typeof elementOrSelector === 'string') {
        return clickSelectorAndWaitForNavigation(page, elementOrSelector, waitOptions);
    }
    else {
        return clickElementAndWaitForNavigation(page, elementOrSelector, waitOptions);
    }
}

export async function clickSelectorAndWaitForNavigation (page: puppeteer.Page, selector: string, waitOptions?: puppeteer.WaitForOptions) {
    return Promise.all([
        page.waitForNavigation(waitOptions),
        page.click(selector)
    ]);
}

export async function clickElementAndWaitForNavigation (page: puppeteer.Page, element: puppeteer.ElementHandle, waitOptions?: puppeteer.WaitForOptions) {
    return Promise.all([
        page.waitForNavigation(waitOptions),
        element.click()
    ]);
}
